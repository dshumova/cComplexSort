//
//  main.c
//  HW-8
//
//  Created by Daria Shumova on 28.11.2017.
//  Copyright © 2017 Daria Shumova. All rights reserved.
//

#include <stdio.h>
#include <mm_malloc.h>

typedef struct {
    int *ptr;
    u_int size;
} array_int;


void swap (int *a, int *b)
{
    *a = *a ^ *b;
    *b = *a ^ *b;
    *a = *a ^ *b;
}

void printArray (array_int *array)
{
    for (int i = 0; i < array->size; i++)
        printf("%i  ", array->ptr[i]);
}

void countingSort (array_int *inputArray, int range)
{
    array_int auxArray;
    auxArray.size = range + 1;
    auxArray.ptr = (int *)malloc(auxArray.size * sizeof(int));
    
    for (int i = 0; i < auxArray.size; i++)
        auxArray.ptr[i] = 0;
    
    for (int i = 0; i < inputArray->size; i++)
        auxArray.ptr[inputArray->ptr[i]]++;
    
    for (int i = 0, j = 0; i < auxArray.size; i++)
    {
        if (auxArray.ptr[i])
        {
            for (int x = 0; x < auxArray.ptr[i]; x++)
            {
                inputArray->ptr[j] = i;
                j++;
            }
        }
    }
    free(auxArray.ptr);
}


int main(int argc, const char * argv[]) {
    
    array_int inputArray = {.ptr = NULL, .size = 10};
    inputArray.ptr = (int *) malloc(inputArray.size * sizeof(int));
    int range = 9;
    
    inputArray.ptr[0] = 5;
    inputArray.ptr[1] = 3;
    inputArray.ptr[2] = 8;
    inputArray.ptr[3] = 1;
    inputArray.ptr[4] = 2;
    inputArray.ptr[5] = 4;
    inputArray.ptr[6] = 5;
    inputArray.ptr[7] = 6;
    inputArray.ptr[8] = 9;
    inputArray.ptr[9] = 3;
    
    printf("Before sort: ");
    printArray(&inputArray);
    
    countingSort(&inputArray, range);
    
    printf("\nAfter sort: ");
    printArray(&inputArray);
    printf("\n");
    free(inputArray.ptr);
    
    return 0;
}
